class TestBugJ1 { // Bug: Allow y to be a boolean expression
    public static void main(String[] args) {
        // If below line compiles successfully then the bug exists
        System.out.println(new Test().testBugJ1());
    }
}

class Test {
    public int testBugJ1() {
        int x;
        boolean y;
        x = 10;
        y = true;
        // The line below should cause a type error in a correct compiler
        x += y;
        return x;
    }
}