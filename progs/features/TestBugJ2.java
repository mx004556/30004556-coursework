class TestBugJ2 { // Bug: Instead of assigning x + y to x, assign y to x
    public static void main(String[] args) {
        System.out.println(new Test().testBugJ2());
    }
}

class Test {
    public int testBugJ2() {
        int result;
        int x;
        int y;
        x = 5;
        y = 10;
        x += y; // Correct behavior: x becomes 15

        // If y is directly assigned to x, x will be 10
        if (x < 11 && 9 < x)
            result = 0; // Test failed
        else
            result = x; // Test passed

        return result;
    }
}