class TestBugJ3 { // Bug: Do not modify x at all
    public static void main(String[] args) {
        System.out.println(new Test().testBugJ3());
    }
}

class Test {
    public int testBugJ3() {
        int result;
        int x;
        int y;
        x = 5;
        y = 20;
        x += y; // Correct behavior: x becomes 25

        // If y is directly assigned to x, x will be 5
        if (x < 6 && 4 < x)
            result = 0; // Test failed
        else
            result = x; // Test passed

        return result;
    }
}